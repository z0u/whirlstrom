#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the playback of sound effects and music.

import aud
import bge
import random
import os

DIRNAME = bge.logic.expandPath("//sounds")

import random
import os

import aud
import bge
listpos = 0


def loop_sound():
    device = aud.device()
    path = os.path.join(DIRNAME, "ambient", "soundtrack.ogg")
    factory = aud.Factory(path)
    handle = device.play(factory)
    try:
        handle.loop_count = -1
    except:
        import traceback
        traceback.print_exc()


def new_fish_sound():
    global listpos
    device = aud.device()
    sounds = [s for s in os.listdir(DIRNAME) if s.endswith(".ogg")]
    random.shuffle(sounds)
    if listpos >= len(sounds):
        random.shuffle(sounds)
        listpos = 0
    path = os.path.join(DIRNAME, sounds[listpos])
    factory = aud.Factory(path)
    handle = device.play(factory)


def death_sound():
    device = aud.device()
    path = os.path.join(DIRNAME, "death", "deathSound.ogg")
    factory = aud.Factory(path)
    handle = device.play(factory)

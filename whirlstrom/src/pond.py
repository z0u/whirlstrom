#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the fish pond, e.g. animated water effects.

import bge

GLSL_LIBS = """
    const float PI2 = 2.0 * 3.14159;

    vec2 displace(out vec2 dx, out vec2 dy) {
        // Big waves use a sine generator.
        vec2 waveAmp = gl_TexCoord[0].st * FREQ_LOW + offsetLow * PI2;
        vec2 disp = sin(waveAmp) * AMPLITUDE_LF;

        // Little waves use a texture. This texture has 3 channels, all of which
        // have different noise (wave) patterns. Only 2 are used for the
        // displacement. Two samples are taken to allow the X and Y dimensions
        // to vary independently.
        vec2 texcoord = gl_TexCoord[0].st * FREQ_HIGH + offsetHigh;
        dx = (texture2D(tDisp, texcoord).rg - 0.5)  * AMPLITUDE_HF;
        texcoord = gl_TexCoord[0].st * FREQ_HIGH - offsetHigh;
        dy = (texture2D(tDisp, texcoord).rg - 0.5)  * AMPLITUDE_HF;
        disp += vec2(dx.x, dy.y);

        return disp;
    }
"""

def wobbly_water_init(c):
    '''Sets up a GLSL shader for animated waves on the water.'''

    verts = """
    void main() {
        // Transfer vertex location and texture coords.
        gl_Position = ftransform();
        gl_TexCoord[0] = gl_MultiTexCoord0;
        gl_TexCoord[1] = gl_MultiTexCoord1;
    }
    """

    fragments = """
    // These uniforms are assigned in Python.
    uniform sampler2D tCol;
    uniform sampler2D tDisp;
    uniform vec2 offsetLow;
    uniform vec2 offsetHigh;

    // This looks odd: low frequency is higher than the high frequency! It's
    // because the high frequency is sourced from a texture, while the low is
    // sourced from a single sine wave.
    const vec2 FREQ_LOW = vec2(30.0, 150.0);
    const vec2 FREQ_HIGH = vec2(5.0, 5.0);
    const float AMPLITUDE_LF = 0.0007;
    //const float AMPLITUDE_LF = 0.00001; // OFF (DEBUG)
    const float AMPLITUDE_HF = 0.007;
    //const float AMPLITUDE_HF = 0.00001; // OFF (DEBUG)

    // Caustics are calculated as the multiplication of 4 components, which are
    // all individually multiplied by AMPLITUDE_HF. Therefore, to bring the
    // caustics value closer to 1, it needs to be multiplied by the inverse of
    // the amplitude raised to the power of 4, where 4 is the number of
    // components.
    //const float CAUSTIC_SCALE = pow(1.0 / (AMPLITUDE_HF * 1.5), 4.0) * 1.5;
    const float CAUSTIC_SCALE = 123405371.21878225;

    """ + GLSL_LIBS + """

    void main() {
        vec2 dx, dy;
        vec2 disp;
        vec4 col;

        disp = displace(dx, dy);

        // Get colour value, displaced by LF and HF waves.
        col = texture2D(tCol, gl_TexCoord[0].st + disp);

        // Add caustics (shimmering spots of light).
        dx += AMPLITUDE_HF;
        dy += AMPLITUDE_HF;
        col += pow(dx.x * dx.y * dy.x * dy.y * CAUSTIC_SCALE, 3.0);

        // Uncomment the next line to enable debugging.
        //col = col*0.0001 + vec4(disp.x, disp.y, 0.0, 1.0) * 100.0 + 0.5;

        gl_FragColor = col;
    }
    """

    ob = c.owner
    me = ob.meshes[0]
    mat = me.materials[0]

    if not hasattr(mat, "getShader"):
        return

    shader = mat.getShader()
    if shader != None:
        if not shader.isValid():
            shader.setSource(verts, fragments, True)
        shader.setSampler("tCol", 0)
        shader.setSampler("tDisp", 1)
    ob["off_low_x"] = 0.0
    ob["off_low_y"] = 0.0
    ob["off_high_x"] = 0.0
    ob["off_high_y"] = 0.0

SPEED_LOWFREQ = (0.01, 0.005)
SPEED_HIGHFREQ = (0.0005, 0.0004)

def wobbly_water_step(c):
    '''Makes the waves move.'''

    ob = c.owner
    me = ob.meshes[0]
    mat = me.materials[0]

    if not hasattr(mat, "getShader"):
        return

    ob["off_low_x"] += SPEED_LOWFREQ[0]
    ob["off_low_x"] %= 1.0
    ob["off_low_y"] += SPEED_LOWFREQ[1]
    ob["off_low_y"] %= 1.0
    ob["off_high_x"] += SPEED_HIGHFREQ[0]
    ob["off_high_x"] %= 1.0
    ob["off_high_y"] += SPEED_HIGHFREQ[1]
    ob["off_high_y"] %= 1.0

    shader = mat.getShader()
    if shader != None:
        # pass another uniform to the shader
        shader.setUniform2f("offsetLow", ob["off_low_x"],  ob["off_low_y"])
        shader.setUniform2f("offsetHigh", ob["off_high_x"],  ob["off_high_y"])


def wobbly_water_spec_init(c):
    '''Sets up a GLSL shader for animated waves on the water.'''

    verts = """
    varying vec3 viewPos;
    varying vec3 normal;
    varying vec3 vertLight;

    void main() {
        // Transfer vertex location and texture coords.
        gl_Position = ftransform();
        gl_TexCoord[0] = gl_MultiTexCoord0;

        viewPos = vec3(gl_ModelViewMatrix * gl_Vertex);
        normal = normalize(gl_NormalMatrix * gl_Normal);
        vertLight = gl_LightSource[0].position.xyz;
    }
    """

    fragments = """
    // These uniforms are assigned in Python.
    uniform sampler2D tDisp;
    uniform vec2 offsetLow;
    uniform vec2 offsetHigh;

    varying vec3 viewPos;
    varying vec3 normal;
    varying vec3 vertLight;

    // This looks odd: low frequency is higher than the high frequency! It's
    // because the high frequency is sourced from a texture, while the low is
    // sourced from a single sine wave.
    const vec2 FREQ_LOW = vec2(100.0, 125.0);
    const vec2 FREQ_HIGH = vec2(4.0, 4.0);
    const float AMPLITUDE_LF = 0.02;
    //const float AMPLITUDE_LF = 0.00001; // OFF (DEBUG)
    const float AMPLITUDE_HF = 0.1;
    //const float AMPLITUDE_HF = 0.00001; // OFF (DEBUG)

    const float SPEC_INTENSITY = 0.4;
    const float SPEC_HARDNESS = 60.0;

    """ + GLSL_LIBS + """

    float specularTerm(vec3 nor) {
        vec3 view;
        vec3 light;

        view = normalize(viewPos);
        light = normalize(vertLight);

        float specular = dot(reflect(light, nor), view);
        specular = pow(specular, SPEC_HARDNESS);
        specular = clamp(specular, 0.0, 1.0);

        return specular;
    }

    void main() {
        vec2 d1, d2;
        vec3 nor;
        vec4 col;
        vec2 disp;

        col = vec4(1.0, 1.0, 0.9, 1.0);

        disp = displace(d1, d2);
        nor = normalize(normal + vec3(disp, 0.0));

        col *= specularTerm(nor) * SPEC_INTENSITY;

        // Uncomment for debugging visualisation.
        //disp += AMPLITUDE_HF + AMPLITUDE_LF;
        //col.rgb = vec3(disp.x, disp.y, 0.0) * 3;
        //col.a = 1.0;

        gl_FragColor = col;
    }
    """

    ob = c.owner
    me = ob.meshes[0]
    mat = me.materials[0]

    if not hasattr(mat, "getShader"):
        ob.visible = False
        return

    shader = mat.getShader()
    if shader != None:
        if not shader.isValid():
            shader.setSource(verts, fragments, True)
        shader.setSampler("tDisp", 0)
    ob["off_low_x"] = 0.0
    ob["off_low_y"] = 0.0
    ob["off_high_x"] = 0.0
    ob["off_high_y"] = 0.0

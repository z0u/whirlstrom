#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the menu scene, e.g. button presses.

import bge

from . import circles
from . import fish
from . import utils
from . import sound

currentBtn = None

S_GAME_SELECT = 2
S_GAME_OVER = 3

def init(c):
    sound.loop_sound()
    if not c.sensors[0].positive:
        return

    bge.render.showMouse(True)

    show_victor(c)
    if is_game_over():
        # Third state is a waiting state.
        c.owner.state = utils.state(S_GAME_OVER)
    else:
        c.owner.state = utils.state(S_GAME_SELECT)


def is_game_over():
    return fish.get_victor() is not None


def show_buttons(c):
    if not c.sensors[0].positive:
        return

    for o in buttons():
        o.setVisible(True, True)
    sce = bge.logic.getCurrentScene()
    sce.objects["menu_title"].setVisible(True, True)

    if circles.is_two_player():
        set_current_button("BtnPlay2")
    else:
        set_current_button("BtnPlay")


def show_victor(c):
    sce = bge.logic.getCurrentScene()
    victor = fish.get_victor()
    if victor == 1:
        sce.addObject("EF_Arm", "EF_Arm")
    else:
        sce.addObject("F_Arm", "F_Arm")


def buttons():
    '''Returns a generator over the buttons in the scene.'''
    sce = bge.logic.getCurrentScene()
    for o in sce.objects:
        if o.name.startswith("Btn"):
            yield(o)

def set_current_button(name):
    global currentBtn

    for o in buttons():
        o.children[0]["show"] = o.name == name
    currentBtn = name

    sound.new_fish_sound()


def next_prev(c):
    direction = None
    if c.sensors["up"].positive:
        direction = "up"
    if c.sensors["down"].positive:
        direction = "down"
    if direction is None:
        return

    sce = bge.logic.getCurrentScene()
    btn = sce.objects[currentBtn]
    nextBtnName = btn[direction]
    set_current_button(nextBtnName)


def start_game(twoPlayer):
    if twoPlayer:
        print("Starting two-player game.")
    else:
        print("Starting one-player game.")

    circles.set_two_player(twoPlayer)
    bge.logic.startGame("//game.blend")


def mouse_over(c):
    if not c.sensors[0].positive:
        return

    set_current_button(c.owner.name)


def start_key(c):
    positive = False
    for s in c.sensors:
        if s.positive:
            positive = True
    if not positive:
        return

    if currentBtn == "BtnPlay":
        start_game(False)
    elif currentBtn == "BtnPlay2":
        start_game(True)
    else:
        bge.logic.endGame()

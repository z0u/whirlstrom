#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the main game play: the motion of the circles. See also:
# fish.py, where fine animation and collisions are handled.

import math

import bge
import mathutils

from . import utils
from . import sound

MAX_LIVES = 4

ONE_EIGHTY = mathutils.Euler((0.0, 0.0, math.radians(180.0)), 'XYZ')
VACANT_FRAMES = 100
# Speed of rotation, in degrees
ROTATION_SPEED = 1.0
ROTATION_SPEED_BOOST_FAC = 3.0
ROTATION_SPEED_BOOST_DFAC = 0.03

RESPAWN_DELAY = 100
EVIL_DELAY = 300
MIN_SPAWN_DIST = 5.0

DEFAULT_Z = 0.1
WHIRLPOOL_OFFSET = 0.01

WHISKER_SPEED = 0.2
WHISKER_OFFSET = 0.05

DEFAULT_TWO_PLAYER = False

_current_player_defaults = {
    "ticks": 0,     # ticker for holding down the key
    "whisker": None,  # GameObject or None
    "whisker_alt": None,  # GameObject or None
    "pool": None,         # GameObject or None
    "lives": MAX_LIVES,   # int
    "boost_ticks": 0,
    "boost_fac": 1,
}

currentPlayers = [None, None]

currentPlayerConfig = [
    _current_player_defaults.copy(),
    _current_player_defaults.copy(),
    ]

log = []


def set_two_player(twoPlayer):
    bge.logic.globalDict["TWO_PLAYER"] = twoPlayer


def is_two_player():
    try:
        return bge.logic.globalDict["TWO_PLAYER"]
    except KeyError:
        bge.logic.globalDict["TWO_PLAYER"] = DEFAULT_TWO_PLAYER
        return DEFAULT_TWO_PLAYER


class LogInfo:
    __slots__ = "start_rot", "end_rot", "scale", "pos"
    def __init__(self, start_rot, end_rot, scale, pos):
        self.start_rot = start_rot
        self.end_rot = end_rot
        self.scale = scale
        self.pos = pos


def get_anchor(circle):
    return circle.children["C_Anchor"]


def get_gauge(circle):
    return circle.childrenRecursive["TimerGauge"]


def get_rot_speed(player_index):
    conf = currentPlayerConfig[player_index]
    if conf["boost_ticks"] > 0:
        conf["boost_ticks"] -= 1
        conf["boost_fac"] = min(
                conf["boost_fac"] + ROTATION_SPEED_BOOST_DFAC,
                ROTATION_SPEED_BOOST_FAC)
    else:
        conf["boost_fac"] = max(
                conf["boost_fac"] - ROTATION_SPEED_BOOST_DFAC, 1)
    return ROTATION_SPEED * conf["boost_fac"]


def get_final_speed(player_index):
    circle = currentPlayers[player_index]
    if circle is None:
        return 0.0
    else:
        return get_rot_speed(player_index) * circle.localScale.x


def create_whirlpool(player_index):

    '''Creates a whirlpool inside a circle. The whirlpool is a separate object
    that will linger after the circle disappears.'''
    circle = currentPlayers[player_index]

    # tag for removal. has own logic to fade away
    if currentPlayerConfig[player_index]["pool"] is not None:
        currentPlayerConfig[player_index]["pool"]["endme"] = 1

    sce = bge.logic.getCurrentScene()
    pool = sce.addObject("WhirlPool", "WhirlPool")

    scale = circle.localScale.copy()
    pos = circle.worldPosition.copy()

    # setup actuator
    pos.z = WHIRLPOOL_OFFSET
    actu = pool.controllers["SpinIn"].actuators["SpinPySetsThis"]

    rot_speed = ROTATION_SPEED * currentPlayerConfig[player_index]["boost_fac"]
    # print(rot_speed)
    if circle["clockwise"]:
        actu.dRot = 0, 0, math.radians(rot_speed)
        scale.y = -scale.y
    else:
        actu.dRot = 0, 0, -math.radians(rot_speed)

    pool.localPosition = pos
    pool.localScale = scale

    currentPlayerConfig[player_index]["pool"] = pool


def get_opposing_spawn_point(circle):
    '''Gets a spawn point that is not too close to the given location.'''
    def dist_key(ob):
        return ob.getDistanceTo(circle)

    sce = bge.logic.getCurrentScene()
    spawnPoints = [ob for ob in sce.objects if "spawnPoint" in ob]
    spawnPoints.sort(key=dist_key)
    for sp in spawnPoints:
        if sp.getDistanceTo(circle) > MIN_SPAWN_DIST:
            return sp
    # None are out of range; return the furthest one.
    return spawnPoints[-1]


def create_next(player_index, scale):
    '''Progresses the player to a new circle, which is adjacent to the current
    one. Essentially, this mirrors the circle across the current location of the
    anchor (i.e. where the fish is).'''

    circle_old = currentPlayers[player_index]

    sce = bge.logic.getCurrentScene()
    if circle_old is None or circle_old.invalid:
        # No current circle exists for this player; create the new circle at the
        # spawn point.
        if is_two_player():
            if player_index == 0:
                other_player = currentPlayers[1]
            else:
                other_player = currentPlayers[0]
            if other_player == None:
                # No one has spawned yet; use 1P spawn point.
                pos = sce.objects["Spawn"].worldPosition
            else:
                pos = get_opposing_spawn_point(other_player).worldPosition
            delay = RESPAWN_DELAY
        else:
            pos = sce.objects["Spawn"].worldPosition
            if player_index == 0:
                delay = RESPAWN_DELAY
            else:
                delay = EVIL_DELAY
        pos.z = DEFAULT_Z
        rot = 0
        clockwise = True
    else:
        # Find the centre of the adjacent (currently imaginary) circle.
        anchor = get_anchor(circle_old)
        vec = anchor.worldPosition - circle_old.worldPosition

        # set the scale of the new circle
        prev_scale = circle_old.localScale.x

        pos = anchor.worldPosition + (vec * (scale / prev_scale))

        rot = (circle_old["rot"] - 180.0) % 360.0
        clockwise = not circle_old["clockwise"]
        delay = 0

    circTemplate = sce.objectsInactive["C_Centre"]
    circle_new = sce.addObject(circTemplate, circTemplate)
    circle_new.worldPosition = pos
    circle_new.localScale = (scale, scale, scale)
    circle_new["rot"] = rot
    circle_new["clockwise"] = clockwise

    update_rot(circle_new, player_index)

    if player_index == 1:
        circle_new["evil"] = True

    circle_new["spawnDelay"] = delay
    circle_new["originalDelay"] = delay
    return circle_new


def kill_evil_fish():
    '''Kills the evil fish (only used in 1-player mode). A new evil fish will be
    created the next time the player moves to a new circle.'''
    reset_fish(1)
    if len(log) > 1:
        log.pop(0)


def create_next_fish(cont, player_index, scale):
    '''Progress the fish to the next circle. This function is closely linked to
    create_next().'''
    oldFish = currentPlayers[player_index]

    currentPlayers[player_index] = create_next(player_index, scale)

    create_whirlpool(player_index)
    if scale > 0.8:
        # Smallest scale is 0.6 or so (as determined by next_scale())
        # This only plays a sound if the circle is fairly large. 
        sound.new_fish_sound()

    if not is_two_player():
        log.append(LogInfo(currentPlayers[0]["rot"], currentPlayers[0]["rot"],
                currentPlayers[player_index].localScale.x,
                currentPlayers[player_index].worldPosition.copy()))

    if oldFish is not None:
        oldFish.endObject()
        if not is_two_player():
            if currentPlayers[1] is None:
                # This is the first jump; queue an evil fish!
                create_next_evil()


def get_anchor_locrot(player_index):
    currentCircle = currentPlayers[player_index]
    anchor = get_anchor(currentCircle)

    pos = anchor.worldPosition.copy()

    heading = 0.0 if currentCircle["clockwise"] else 180.0
    orn = anchor.worldOrientation.to_quaternion()
    orn.rotate(mathutils.Euler((0.0, 0.0, math.radians(heading)), 'XYZ'))

    return pos, orn


def update_whisker(player_index):

    if currentPlayers[player_index] is None:
        return

    # update whisker
    factor = WHISKER_SPEED
    sce = bge.logic.getCurrentScene()
    if currentPlayerConfig[player_index]["whisker"] is None:  # first time
        w = sce.addObject("Whisker", "Whisker")
        currentPlayerConfig[player_index]["whisker"] = w
        w.children[0].color = (0.0, 0.0, 0.0, 1.0)
        factor = 1.0
    if currentPlayerConfig[player_index]["whisker_alt"] is None:  # first time
        w = sce.addObject("Whisker", "Whisker")
        currentPlayerConfig[player_index]["whisker_alt"] = w
        w.children[0].color = (0.0, 0.0, 0.0, 1.0)
        factor = 1.0

    # assign vars
    circle = currentPlayers[player_index]
    is_clockwise = circle["clockwise"]
    whisker = currentPlayerConfig[player_index]["whisker"]
    whisker_alt = currentPlayerConfig[player_index]["whisker_alt"]
    if not is_clockwise:
        whisker, whisker_alt = whisker_alt, whisker

    # setup whisker
    pos, orn = get_anchor_locrot(player_index)
    pos -= mathutils.Vector((0, 0, WHISKER_OFFSET))
    whisker.localOrientation = whisker_alt.localOrientation = orn
    whisker.localPosition = whisker_alt.localPosition = pos

    scale = next_scale(player_index)
    if not is_clockwise:
        scale = -scale
    whisker["size"] = utils.lerp(whisker["size"], scale * 100.0, factor)

    scale = circle.localScale.x
    if is_clockwise:
        scale = -scale
    whisker_alt["size"] = utils.lerp(
            whisker_alt["size"], scale * 100.0, factor)


def create_next_fish_delay(cont, player_index):
    '''Called in response to a button push. The "delay" part is the user holding
    the button down: while it's down, the next circle is scaled up.'''
    circle = currentPlayers[player_index]
    if cont.sensors[0].positive:
        if circle is not None:
            currentPlayerConfig[player_index]["ticks"] += 1
        else:
            currentPlayerConfig[player_index]["ticks"] = 0
    else:
        if circle is not None and circle["spawnDelay"] <= 0:
            create_next_fish(cont, player_index, next_scale(player_index))
        currentPlayerConfig[player_index]["ticks"] = 0


def create_next_fish_delay_p1(cont):
    create_next_fish_delay(cont, 0)


def create_next_fish_delay_p2(cont):
    create_next_fish_delay(cont, 1)


def next_scale(player_index):
    return 0.5 + (currentPlayerConfig[player_index]["ticks"] / 50.0)


def create_next_evil(cont=None):
    '''Create a fish that follows the leader. Only used in 1-player game.'''
    if cont is not None and cont.sensors[0].positive:
        return

    if currentPlayers[1] is not None:
        log.pop(0)

    if len(log) > 0:
        scale = log[0].scale
        pos = log[0].pos
    else:
        scale = 1.0
        pos = (0.0, 0.0, 0.0)

    currentPlayers[1] = create_next(1, scale)
    currentPlayers[1].worldPosition = pos
    create_whirlpool(1)

    if len(log) > 0:
        logInfo = log[0]
        totRot = logInfo.end_rot - logInfo.start_rot
        if len(log) > 1:
            # This makes the Evil take the shortest path available.
            currentPlayers[1]["clockwise"] = (totRot % 360.0) < 180.0
        else:
            # Evil is one the same circle as the fish, so go the other way to
            # force a collision.
            currentPlayers[1]["clockwise"] = not currentPlayers[0]["clockwise"]
        currentPlayers[1].localScale = (scale, scale, scale)


# only run once at the start
def init(cont):
    bge.render.showMouse(False)
    sound.loop_sound()
    print("Two player:", is_two_player())
    if not is_two_player():
        sce = bge.logic.getCurrentScene()
        sce.objects["Player2"].endObject()


def start(cont=None):
    '''Resets and respawns the fish.'''
    if cont is not None and not cont.sensors[0].positive:
        return

    reset()

    spawn_fish(0)
    if is_two_player():
        spawn_fish(1)


def spawn_fish(player_index):
    create_next_fish(None, player_index, 1.0)


def reset_whiskers(player_index):
    if currentPlayerConfig[player_index]["whisker"] is not None:
        currentPlayerConfig[player_index]["whisker"].endObject()
        currentPlayerConfig[player_index]["whisker"] = None
    if currentPlayerConfig[player_index]["whisker_alt"] is not None:
        currentPlayerConfig[player_index]["whisker_alt"].endObject()
        currentPlayerConfig[player_index]["whisker_alt"] = None


def reset_fish(player_index):
    if currentPlayers[player_index] is not None:
        currentPlayers[player_index].endObject()
        currentPlayers[player_index] = None
    currentPlayerConfig[player_index]["boost_ticks"] = 0
    currentPlayerConfig[player_index]["boost_fac"] = 1
    reset_whiskers(player_index)


def reset():
    reset_fish(0)
    reset_fish(1)

    log[:] = []


def update_rot(circ, player_index):
    '''This function defines the movement of a fish around a single circle.
    @return: A 2-tuple containing the new and old orientation (in degrees around
    the z-axis). The returned value for the new rotation is NOT limited to the
    range 0-360.'''

    rot_speed = get_rot_speed(player_index)

    if circ["clockwise"]:
        dRot = -rot_speed
    else:
        dRot = rot_speed

    old_rot = circ["rot"]
    rot = old_rot - dRot
    # Store bounded rotation.
    circ["rot"] = rot % 360.0

    orn = mathutils.Euler((0.0, 0.0, math.radians(circ["rot"])), 'XYZ')
    circ.localOrientation = orn

    # Return unbounded rotation.
    return (old_rot, rot)


def update_respawn_gauge(player_index):
    circle = currentPlayers[player_index]
    delay = circle["spawnDelay"]

    if delay > 0:
        try:
            gauge = get_gauge(circle)
        except KeyError:
            sce = bge.logic.getCurrentScene()
            anchor = get_anchor(circle)
            gauge = sce.addObject("TimerGauge", anchor)
            gauge.setParent(anchor)
            gauge.localPosition = (0.0, 0.0, 0.0)
            gauge.color = (1.0, 1.0, 1.0, 0.0)

        progress = 100.0 - (delay / circle["originalDelay"]) * 100.0
        gauge["frame"] = progress

    elif delay == 0:
        try:
            gauge = get_gauge(circle)
            gauge.endObject()
        except KeyError:
            pass


def update(cont):
    '''Updates a single circle. Called by circle logic brick, trigger'''
    own = cont.owner

    player_index = (1 if own["evil"] else 0)
    if currentPlayers[player_index] is not own:
        # This circle has just been destroyed and replaced, either with a new
        # circle, or nothing. But one more update impulse may come through on
        # the same frame!
        assert("_dying" not in own)
        own["_dying"] = True
        return
    if is_two_player():
        assert(len(log) == 0)
    else:
        assert(len(log) > 0)

    if own["spawnDelay"] >= 0:
        update_respawn_gauge(player_index)
        own["spawnDelay"] -= 1
        return

    old_rot, rot = update_rot(own, player_index)

    if is_two_player():
        update_whisker(player_index)
        return

    if player_index == 0:
        update_whisker(player_index)

    if own["evil"]:
        # Check current rotation and progress to next circle if necessary.

        # This funky calculation is required because the angular speed may not
        # be an integer; in that case, the evil fish is unlikely to ever have
        # same rotation as the good fish's last rotation. So what we're looking
        # for is the condition old_rot <= end_rot <= current_rot. But things are
        # complicated because of the wrapping nature (modulo) of the circle.
        lower_bound = min(old_rot, rot)
        upper_bound = max(old_rot, rot)
        end_rot = log[0].end_rot

        if lower_bound < 0:
            # This means the fish has crossed over the zero point going CCW.
            passed = (lower_bound % 360.0) <= end_rot
        elif upper_bound > 360.0:
            # The fish has crossed over the zero point going CW.
            passed = (upper_bound % 360.0) >= end_rot
        else:
            # Zero point not crossed; just do basic a <= b <= c test.
            passed = lower_bound <= end_rot and upper_bound >= end_rot

        if passed:
            create_next_evil(None)
            own.endObject()
    else:
        # Update the log. This lets the evil fish follow.
        logInfo = log[-1]  # sometimes empty FIXME
        logInfo.end_rot = own["rot"]

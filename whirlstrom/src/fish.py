#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the location, animation and collisions of the fish. Some
# level logic is here too, e.g. if all the fish die then the game ends. The
# gross motion of the fish is not defined here; see circles.py instead.

import math

import bge
import mathutils

from . import sound
from . import circles
from . import utils

# Trail spacing and life are measured in logic ticks.
TRAIL_SPACING = 8
TRAIL_LIFE = 600
# Frames per tick for a unit circle.
NORMAL_ANIM_SPEED = 2
ORIENTATION_SPEED = 0.2

BOOST_TICKS = 300

GOOD_COLOUR = (1, 1, 0.8, 1)
EVIL_COLOUR = (0.01, 0.01, 0.02, 1)

Y_AXIS = mathutils.Vector((0, 1, 0))
DEAD_FISH_OFFSET = -200

S_ALIVE = 1

def start():
    circles.currentPlayerConfig[0]["lives"] = circles.MAX_LIVES
    circles.currentPlayerConfig[1]["lives"] = circles.MAX_LIVES


def set_victor(fish_id):
    bge.logic.globalDict["VICTOR"] = fish_id


def get_victor():
    try:
        return bge.logic.globalDict["VICTOR"]
    except KeyError:
        bge.logic.globalDict["VICTOR"] = None
        return None


def update_fish(cont):
    fish = cont.owner
    player_index = (1 if fish["evil"] else 0)

    currentCircle = circles.currentPlayers[player_index]

    if currentCircle is None or currentCircle["spawnDelay"] > 0:
        # Get the fish out of the way so it doesn't accidentally collide with
        # anything.
        fish.worldPosition.z = DEAD_FISH_OFFSET * (player_index + 1)

        # also kick associated objects, these can be removed
        # since they get added back as needed
        circles.reset_whiskers(player_index)

        return

    if circles.currentPlayerConfig[player_index]["lives"] <= 0:
        game_over()
        return

    config = circles.currentPlayerConfig[player_index]
    dfra = NORMAL_ANIM_SPEED * currentCircle.localScale.x * \
            config["boost_fac"]
    fish["frame"] = (fish["frame"] + dfra) % 41

    pos, t_orn = circles.get_anchor_locrot(player_index)
    fish.localPosition = pos
    t_orn.rotate(mathutils.Euler((0.0, 0.0, math.radians(90.0)), 'XYZ'))
    orn = fish.localOrientation.to_quaternion()
    fish.localOrientation = orn.slerp(t_orn, ORIENTATION_SPEED)

    # Create a trail of bubbles.
    fish["trailFrame"] += 1
    if fish["trailFrame"] > TRAIL_SPACING:
        sce = bge.logic.getCurrentScene()
        crumb = sce.addObject("Breadcrumb", fish, TRAIL_LIFE)
        if fish["evil"]:
            crumb.color = EVIL_COLOUR
            # Nudge it a bit to make it draw on top.
            crumb.worldPosition.z -= circles.DEFAULT_Z * 0.5
        else:
            crumb.color = GOOD_COLOUR
            crumb.worldPosition.z -= circles.DEFAULT_Z * 0.6
        fish["trailFrame"] = 0


def _collision_respawn(fish, player_index):
    circles.currentPlayerConfig[player_index]["lives"] -= 1
    # Get fish out of the way of whatever it hit, to avoid double
    # -kills.
    fish.worldPosition.z = DEAD_FISH_OFFSET * (player_index + 1)

    circles.reset_whiskers(player_index)

    fish.sendMessage("Death")


def collision(cont):
    s = cont.sensors[0]
    if s.positive:

        # check if we collided player to player
        fish = cont.owner

        # Is this other a player? Note: we need to check the parent, because the
        # collision object is a child.
        if s.hitObject.parent is not None and "player" in s.hitObject.parent:
            other_fish = s.hitObject.parent
            if 1:
                y_own = other_fish.getAxisVect(Y_AXIS)
                y_other = fish.getAxisVect(Y_AXIS)

                delta = other_fish.worldPosition - fish.worldPosition
                dot_pos = y_own.dot(delta)    # position, located infront of us
                dot_ang = y_own.dot(y_other)  # are they facing away from us

                if dot_pos > 0.0 and dot_ang > 0.0:
                    # whee!, we hit from the right angle
                    # the other fish will likely die
                    return
            else:
                # TODO? ram mode? - disabled for now but fun!
                player_index = (1 if fish["evil"] else 0)
                own_speed = circles.get_final_speed(player_index)
                other_speed = circles.get_final_speed(not player_index)
                if own_speed > other_speed:
                    return  # we win speed test!

        sce = bge.logic.getCurrentScene()
        sound.death_sound()
        sce.addObject("InkSplat", fish, 1000)

        if circles.is_two_player():
            player_index = (1 if fish["evil"] else 0)

            # Two player; reset just this fish.
            _collision_respawn(fish, player_index)
            circles.reset_fish(player_index)
            circles.spawn_fish(player_index)
        else:
            if fish["evil"]:
                # Evil fish died in single player mode.
                circles.kill_evil_fish()
            else:
                # Single player; reset both fish, but only respawn this one.
                _collision_respawn(fish, 0)
                circles.reset()
                circles.start()


def get_num_prawns():
    numprawns = 0
    for ob in bge.logic.getCurrentScene().objects:
        #print(ob.name)
        if ob.name == "Prawn":
            numprawns += 1
    return numprawns


def collision_pickup(cont):
    s = cont.sensors[0]
    if s.positive:

        # check if we collied player to player
        fish = cont.owner
        other = s.hitObject

        other.endObject()

        if (not circles.is_two_player()) and get_num_prawns() <= 1:
            # Eaten all the prawns. Note that at this point the object has only
            # been flagged for deletion, so get_num_prawns will return 1 when
            # the last one is eaten.
            game_over()

        player_index = (1 if fish["evil"] else 0)
        circles.currentPlayerConfig[player_index]["boost_ticks"] = BOOST_TICKS


def game_over():
    lives = (
        circles.currentPlayerConfig[0]["lives"],
        circles.currentPlayerConfig[1]["lives"],
    )

    if circles.is_two_player():
        # Two-player mode: player with the most lives wins.
        if lives[0] == lives[1]:
            set_victor(None)
        elif lives[0] > lives[1]:
            set_victor(0)
        else:
            set_victor(1)
    else:
        # Single-player mode: player has won if they have any lives left.
        if lives[0] > 0:
            set_victor(0)
        else:
            set_victor(1)
    bge.logic.startGame("//menu.blend")

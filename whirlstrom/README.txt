1. Install Blender 2.76: http://download.blender.org/release/Blender2.76/
2. Unzip whirlstrom.zip
3. Double-click on whirlstrom/menu.blend

Alternatively, start Blender, choose File > Open, browse to
whirlstrom/menu.blend, and press P to start the game.

Player one: spacebar.
Player two: return.

Press button to flip the circle you are moving around over the fish. Hold to
change the size of the next circle.

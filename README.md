# Whirlstrom

In Whirlstrom, you are a fish swimming in a large pond. The aim is to eat the
prawns - but watch out for the Evil Fish, who relentlessly tracks you down!

This is a chilled-out 2D game with a Zen air.

Runs on desktop computers (Windows, Mac, GNU+Linux) in the Blender game engine.
